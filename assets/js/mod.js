function toTop() {
	$('body,html').animate({
		scrollTop: 0
	}, 1000);
	return false;
}

/*----------  NAVIGATION ON PAGE  ----------*/
	var navHeight = $('#main-menu-navigation').outerHeight();
	$(document).on('click', '.nav-link', function(){
		var id = $(this).attr('href');
		$('body,html').animate({
			scrollTop: $(id).offset().top-navHeight
		}, 1000);
  });

	$(window).scroll(function(){
    // Detect how far are we from the top of the page
		var windowTop = $(this).scrollTop();
		// Loop through every navigation menu item
		$('#main-menu-navigation .op-link').each(function (event) {
			var ids = $(this).attr('href');
	    if (windowTop >= $(ids).offset().top-navHeight-10) {
        // Remove 'active' from previously highlighted menu items
				$('#main-menu-navigation .nav-item').removeClass('active');
        // Highlight the current menu item by adding 'active' class
        $(this).parent().addClass('active');
	    }
		});
	});

/*----------  //NAVIGATION ON PAGE  ----------*/

$('.menu-item').each(function(){
	$(this).hover( 
	  function() {
	    $(this).find('.position').addClass('fadeInDown animated');
	    $(this).find('.food-name').addClass('fadeInDown animated');
	    $(this).find('.price').addClass('fadeInUp animated');
	  },
	  function() {
	    $(this).find('.position').removeClass('fadeInDown animated');
	    $(this).find('.food-name').removeClass('fadeInDown animated');
	    $(this).find('.price').removeClass('fadeInUp animated');
	  }
	);
});

$('.galery-item').each(function(){
	$(this).hover( 
	  function() {
	    $(this).find('.position').addClass('fadeInDown animated');
	    $(this).find('.food-name').addClass('fadeInDown animated');
	    $(this).find('.price').addClass('fadeInUp animated');
	  },
	  function() {
	    $(this).find('.position').removeClass('fadeInDown animated');
	    $(this).find('.food-name').removeClass('fadeInDown animated');
	    $(this).find('.price').removeClass('fadeInUp animated');
	  }
	);
});

$('.menu-item').on('click', function(){
	$('#popup').show().css('opacity','1');
});

$('.close-button').on('click', function(){
	$('#popup').hide().css('opacity','0');
	$('#gallery-popup').hide().css('opacity','0');
});

$('.galery-item').on('click', function(){
	$('#galery').show().css('opacity','1');
});


var topNavHeight 		= $('#navbar-mobile').outerHeight();
var mainMenuHeight	= $('.main-menu-content').outerHeight();
var topMainHeight		= topNavHeight + mainMenuHeight;
$('.flexslider img').css('max-height','calc(100vh - '+topMainHeight+'px)');

var image = document.getElementsByClassName('parallax');
new simpleParallax(image);


$(document).ready(function(){
	$('.parallax').parallax("50%", 0);
	var winLoc = location.hash;
	if (winLoc == ''){
		//alert('kosong');
	} else {
		$('body,html').animate({
			scrollTop: $(winLoc).offset().top-navHeight
		}, 1000);
	}
})

// Initiate the wowjs
new WOW().init();

if ($('.wow').visibility == "visible") {
  $('.wow').css('opacity','1');
}

$('.flexslider').flexslider({
  animation: "fade",
  before: function(slider){
    $(slider).find(".flex-active-slide").find('.big').each(function(){
    	$(this).removeClass("animated fadeInDown");
    });
    $(slider).find(".flex-active-slide").find('.middle').each(function(){
    	$(this).removeClass("animated fadeInDown");
    });
    $(slider).find(".flex-active-slide").find('p').each(function(){
    	$(this).removeClass("animated fadeInUp");
    });
   },
  after: function(slider){
    $(slider).find(".flex-active-slide").find('.middle').addClass("animated fadeInDown");
    $(slider).find(".flex-active-slide").find('.big').addClass("animated fadeInDown");
    $(slider).find(".flex-active-slide").find('p').addClass("animated fadeInUp");
  },
});

$('#dish').on('click', function(){
	$(this).addClass("active");
	$('#dessert').removeClass("active");
	$('#drink').removeClass("active");
	$('#showDish').show();
	$('#showDessert').hide();
	$('#showDrink').hide();
});

$('#dessert').on('click', function(){
	$(this).addClass("active");
	$('#dish').removeClass("active");
	$('#drink').removeClass("active");
	$('#showDessert').removeClass("d-none");
	$('#showDish').hide();
	$('#showDessert').show();
	$('#showDrink').hide();
});

$('#drink').on('click', function(){
	$(this).addClass("active");
	$('#dish').removeClass("active");
	$('#dessert').removeClass("active");
	$('#showDrink').removeClass("d-none");
	$('#showDish').hide();
	$('#showDessert').hide();
	$('#showDrink').show();
});


$('#gallery a.img, #gallery a.name').on('click', function(){
	$('#gallery-popup').show().css('opacity','1');
});
